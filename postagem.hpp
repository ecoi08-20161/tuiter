#ifndef POSTAGEM_HPP
#define POSTAGEM_HPP

#include <cstring>
#include <string>
#include "timestamp.hpp"

class usuario;

class postagem
{
	private:
		char      m_texto[160];
		TimeStamp m_timestamp;
		usuario*  m_parent;

	public:
	    ///GETTERS E SETTERS
        postagem();
        ~postagem();

        char*     getText();
		usuario*  getParent();
		TimeStamp getTimestamp();

		void     setText(char[]);
		void     setParent(usuario*);
		void     setTimestampData(TimeStamp::TimeStampData);
};

#endif
