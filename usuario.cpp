#include <cstdio>
#include "usuario.hpp"
#include "postagem.hpp"


int usuario::m_staticid = 0;

usuario::usuario() {
    m_id = m_staticid++;
    numPostagens = 0;
}

usuario::~usuario()
{
	for (auto ptrPost : m_postlist)
		delete ptrPost;
}



///GETTERS E SETTERS
void usuario::setUsername(std::string _Username)
{
    m_username = _Username;
}
void usuario::setEncryptedPassword(std::string _Password)
{
    m_encryptedpass = _Password;
}
void usuario::setStaticID(int n) {
    m_staticid = n;
}

void usuario::printTimeline()
{
	printf("@%s posts:\n", m_username.c_str());
	for (auto ptrMsg : m_postlist)
	{
		printf("%s\t%s\n",
			ptrMsg->getTimestamp().toString().c_str(),
			ptrMsg->getText()
			);
	}
	printf("\n");
}

void usuario::addMention(postagem* mention) {
	m_mentions.insert(m_mentions.begin(), mention);
}

void usuario::printMentions()
{
	printf("@%s mentions:\n", m_username.c_str());
	for (auto ptrMsg : m_mentions)
	{
		printf("%s\t@%s: %s\n",
			ptrMsg->getTimestamp().toString().c_str(),
			ptrMsg->getParent()->getUsername().c_str(),
			ptrMsg->getText()
			);
	}
	printf("\n");
}

int usuario::getID() {
	return m_id;
}

std::string usuario::getUsername() {
	return m_username;
}

std::string usuario::getEncryptedPassword() {
	return m_encryptedpass;
}

int usuario::getStaticID() {
    return m_staticid;
}

const std::vector<postagem*>* usuario::getPostList()
{
	return &m_postlist;
}

postagem* usuario::postarMensagem(char *mensagem)
{
    postagem* NewMessage= new postagem;
    NewMessage->setText(mensagem);
	NewMessage->setParent(this);

    m_postlist.insert(m_postlist.begin(), NewMessage);
	return NewMessage;
}
