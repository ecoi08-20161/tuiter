#pragma once

#include <string>

struct TimeStamp
{
    struct TimeStampData
    {
        unsigned int day,
                     month,
                     year,
                     hour,
                     minute;
    };

    TimeStampData timeStamp;

    TimeStamp();
    void getTime();

    TimeStamp& operator=(const TimeStamp&);
    bool operator>(const TimeStamp&);
    bool operator<(const TimeStamp&);
    bool operator>=(const TimeStamp&);
    bool operator<=(const TimeStamp&);
    bool operator==(const TimeStamp&);

    std::string toString();
};
