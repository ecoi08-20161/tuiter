#pragma once

#include <string>
#include <iostream>
#include <cstdlib>
#include "usuario.hpp"

class AVLTree
{
public:
	struct Node
	{
		std::string key;
		usuario* info;
		int fb = 0;
		Node* left = nullptr,
			*right = nullptr;
	};

private:
	// N� raiz
	Node* root;

	// N�mero de elementos
	unsigned int n_elems;

	// Limpa recursivamente
	inline void _recursiveClear(Node*& n);

	// Pesquisa
	inline usuario* _find(std::string key, Node* n);

	// Inser��o
	inline void _add(Node*& n, std::string key, usuario* info);

	// Remo��o
	inline void _remove(Node*& n, std::string key);

	// Dele��o de n�
	inline void _deleteNode(Node*& n);
	long long int _getHeight(const Node* n);

	// Balanceamento
	inline void _evaluateBalanceFactor(Node*& n);
	inline void _evaluateFactorsRecursively(Node*& n);
	inline void _balance(Node*& n);

	// Rota��es
	inline void _rightRotation(Node*& n);
	inline void _leftRotation(Node*& n);
	inline void _doubleRightRotation(Node*& n);
	inline void _doubleLeftRotation(Node*& n);

public:
	AVLTree();
	~AVLTree();
	void Add(std::string key, usuario* info);
	void Remove(std::string key);
	void Clear();
	usuario* Find(std::string key);
	bool IsEmpty();
	unsigned int size();
	Node* getRoot();
};