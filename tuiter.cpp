#include "tuiter.hpp"
#include "postagem.hpp"
#include <cstdio>
#include <string>
#include <ctime>
#include <cstring>
#include <algorithm>
#include <set>
#include <list>
#include "savetool.hpp"


tuiter::tuiter()
{
	m_loggedInUser = NULL;
	SaveTool::Load("data.dat", (*this));
}

tuiter::~tuiter()
{
	SaveTool::Save("data.dat", m_usertree);
	m_usertree.Clear();
}

bool tuiter::registerUser(std::string username, std::string pass)
{
	if(m_usertree.Find(username)) {
		return false;
	}

	usuario* usr = new usuario;
	usr->setUsername(username);
	usr->setEncryptedPassword(pass);

	m_usertree.Add(username, usr);
	return true;
}

bool tuiter::login(std::string username, std::string pass)
{
	usuario* usr = m_usertree.Find(username);
	if(!usr) return false;

	if (usr->getEncryptedPassword() == pass)
	{
		m_loggedInUser = usr;
		return true;
	}

	return false;
}

void tuiter::logoff()
{
	m_loggedInUser = nullptr;
}

void tuiter::post(char* msg)
{
	if (!m_loggedInUser) {
		printf("ERROR: No user logged in\n");
		return;
	}

	postagem* post = m_loggedInUser->postarMensagem(msg);
	indexMentionsAndHashtags(post);
}

void tuiter::repost(char* msg, TimeStamp::TimeStampData tsd)
{
	if (!m_loggedInUser) return;
	postagem* post = m_loggedInUser->postarMensagem(msg);
	post->setTimestampData(tsd);
	indexMentionsAndHashtags(post);
}

std::string tuiter::loggedInAs() {
	return m_loggedInUser == nullptr ? "NONE" : m_loggedInUser->getUsername();
}

bool tuiter::isLoggedIn() {
	return m_loggedInUser != nullptr;
}

usuario* tuiter::getActiveUser() const
{
	return m_loggedInUser;
}

void tuiter::indexMentionsAndHashtags(postagem* myMessage)
{
	const char delimiters[] = " ,.#-_/\\&*?!@$()[]{}";
	char* ptr = strchr(myMessage->getText(), '@');
	while (ptr != nullptr)
	{
		ptr++;
		char* mentionEnd = ptr;

		while ([&]() -> bool
		{
			if ((*mentionEnd) == '\0') return false;
			for (unsigned int i = 0; i < strlen(delimiters); i++)
				if (delimiters[i] == *mentionEnd) return false;
			return true;
		}()) mentionEnd++;

		// Agora que achamos nosso usu�rio, vamos montar o nome.
		std::string myUsername;
		while (ptr != mentionEnd) {
			myUsername.push_back(*ptr);
			ptr++;
		}

		// Procure pela lista de usu�rios! Caso ele exista,
		// adicione uma men��o a ele neste post.
		// Sugest�o de otimiza��o: usu�rios armazenados
		// em �rvore. Talvez pelo nome.
		{
			usuario* usr = m_usertree.Find(myUsername);
			if(usr) usr->addMention(myMessage);
			ptr = strchr(ptr, '@');
		}
	}

	// Hashtags
	// O sistema de hashtags � similar ao sistema de men��es.
	ptr = strchr(myMessage->getText(), '#');
	while (ptr != nullptr)
	{
		ptr++;
		char* hashtagEnd = ptr;
		while ([&]() -> bool
		{
			if ((*hashtagEnd) == '\0') return false;
			for (unsigned int i = 0; i < strlen(delimiters); i++)
				if (delimiters[i] == *hashtagEnd) return false;
			return true;
		}()) hashtagEnd++;

		std::string myHashtag;
		while (ptr != hashtagEnd) {
			myHashtag.push_back(*ptr);
			ptr++;
		}
		// Hashtags devem ser indexadas em lowercase
		// Infelizmente vamos ter que iterar manualmente por cada caractere.
		// Por sorte, STL facilita nossa vida
		std::transform(myHashtag.begin(), myHashtag.end(), myHashtag.begin(), tolower);

		// Agora que temos nossa hashtag, comparemos se ela existe
		// no �ndice.

		// Caso a hashtag n�o tenha sido registrada no �ndice
		if (m_hashtags.find(myHashtag) == m_hashtags.end())
		{
			// Crie o vector de postagens com ocorr�ncias
			std::vector<postagem*> myHashtagOccurrences;
			// Adicione-a. Posi��o � irrelevante
			myHashtagOccurrences.push_back(myMessage);
			// Armazene no map
			m_hashtags[myHashtag] = myHashtagOccurrences;
		}
		// Caso tenha sido registrada no �ndice
		else {
			// Confira se n�o h� duplicatas da mensagem a ser
			// inserida
			bool duplicateExists;
			for (auto ptrMsg : m_hashtags[myHashtag])
				if (ptrMsg == myMessage) {
					duplicateExists = true;
					break;
				}

			// Se n�o h� duplicatas, ent�o apenas insira no in�cio da lista
			if(!duplicateExists)
				m_hashtags[myHashtag].insert(m_hashtags[myHashtag].begin(), myMessage);
		}

		ptr = strchr(ptr, '#');
	}
}

//unsigned int tuiter::generateStringHash(const char* string, int hash) {
//	return !string[hash] ? 5381 : (generateStringHash(string, hash + 1) * 33) ^ string[hash];
//}

void tuiter::printHashtags(std::string hashtag)
{
	std::string hashtaglwr = hashtag;

	// Procuramos pela forma da hashtag em lowercase
	std::transform(hashtaglwr.begin(), hashtaglwr.end(), hashtaglwr.begin(), tolower);

	// Se n�o houver nenhuma hashtag...
	if(m_hashtags.find(hashtaglwr) == m_hashtags.end()) {
		printf("\tError: no hashtag \"#%s\" found.\n\n", hashtag.c_str());
		return;
	}

	// Se houver, ent�o poste cada uma das ocorr�ncias formatadamente
	printf("Showing all posts with #%s:\n", hashtag.c_str());
	for (auto ptrMsg : m_hashtags[hashtaglwr])
	{
		printf("%s\t@%s: %s\n",
			ptrMsg->getTimestamp().toString().c_str(),
			ptrMsg->getParent()->getUsername().c_str(),
			ptrMsg->getText()
			);
	}
	printf("\n");
}

void tuiter::search(std::string searchText)
{
	printf("Please be patient, this could take a while...\n");
	std::string searchTextLwr = searchText;
	std::transform(searchTextLwr.begin(), searchTextLwr.end(), searchTextLwr.begin(), tolower);
	bool found = false;

	// Encontra nomes de usuários contendo substring
	printf("Users containing text \"%s\":\n", searchText.c_str());
	usuario* ptrUsr = m_usertree.Find(searchText);
	if(ptrUsr) printf("\t@%s\n", ptrUsr->getUsername().c_str());
	else       printf("\tNo users found.\n");
	printf("\n");
}


