#ifndef USUARIO_HPP
#define USUARIO_HPP

#include <string>
#include <vector>
#include <cstdint>
#include "postagem.hpp"

class usuario
{
	private:
        static int  m_staticid;
		int 		m_id;
		std::string m_username;
		std::string m_encryptedpass;
		// Lista de postagens
		std::vector<postagem*> m_postlist;
		// Lista de men��es
		std::vector<postagem*> m_mentions;
		// Ato de postar com o numero de postagens.
        int numPostagens;

	public:
        usuario();
		~usuario();
	    ///GETTERS E SETTERS
	    void        setUsername(std::string);
	    void        setEncryptedPassword(std::string);
	    postagem*   postarMensagem(char*);
        static void setStaticID(int);
		void        printTimeline();
		void        addMention(postagem*);
		void        printMentions();


		int         getID();
		std::string getUsername();
		std::string getEncryptedPassword();
		static int  getStaticID();
		const std::vector<postagem*>* getPostList();

};

#endif
