#pragma once

#include "timestamp.hpp"
#include "tuiter.hpp"
#include "avltree.hpp"

class SaveTool
{
public:
	struct UserSave
	{
		char username[80];
		char password[80];
		unsigned int n_posts = 0u;
	};

	struct PostSave
	{
		char message[160];
		TimeStamp::TimeStampData ts;
	};

	static void Save(const char* file, AVLTree& myTree);
	static void Load(const char* file, tuiter& t);
};