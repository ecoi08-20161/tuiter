#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "tuiter.hpp"

#ifndef FLUSH_STRING
	#if defined(_WIN32)
		#define FLUSH_STRING "cls"
	#else
		#define FLUSH_STRING "clear"
	#endif
#endif

tuiter m_tuiter;

int main(void)
{
	std::string buffer, buffer2;
	char mensagem[160];
	int option;

	//std::ios::sync_with_stdio(false);

	do
	{
		printf("Tuiter\n");
		if (m_tuiter.isLoggedIn())
			printf("Logado como %s\n", m_tuiter.loggedInAs().c_str());
		printf("\n");
		printf("\t1. Registrar usuario\n"
			"\t2. Login\n"
			"\t3. Tuitar...\n"
			"\t4. Ver timeline\n"
			"\t5. Ver mencoes\n"
			"\t6. Pesquisar por hashtag...\n"
			"\t7. Pesquisar por texto...\n"
			"\t8. Logoff\n"
			"\t9. Auto-teste\n"
			"\t0. Sair\n\n"
			"Opcao: ");

		scanf("%d", &option);
		std::cin.ignore();
		fflush(stdin);

		switch (option)
		{
		case 1:
			printf("Digite o nome de usuario a registrar: ");
			std::cin >> buffer;
			printf("Digite a sua senha: ");
			std::cin >> buffer2;
			system(FLUSH_STRING);
			if (m_tuiter.registerUser(buffer, buffer2))
				printf("Registrado com sucesso.\n");
			else printf("Erro: Voce ja esta registrado.\n");
			break;

		case 2:
			printf("Digite o nome de usuario: ");
			std::cin >> buffer;
			printf("Digite a sua senha: ");
			std::cin >> buffer2;
			system(FLUSH_STRING);
			if (m_tuiter.login(buffer, buffer2))
				printf("Login feito com sucesso.\n");
			else printf("Erro: Voce nao esta registrado.\n");
			break;

		case 3:
			if (!m_tuiter.isLoggedIn()) {
				printf("Erro: Voce nao fez login.\n");
				break;
			}

			printf("Digite o seu tweet (maximo de 160 caracteres):\n"
				   ">> ");
			std::cin.getline(mensagem, 160);
			//fflush(stdin);
			m_tuiter.post(mensagem);
			//std::cin.ignore();
			system(FLUSH_STRING);
			printf("Postado com sucesso.\n");
			break;

		case 4:
			system(FLUSH_STRING);
			if (!m_tuiter.isLoggedIn()) {
				printf("Erro: Voce nao fez login.\n");
				break;
			}

			m_tuiter.getActiveUser()->printTimeline();
			break;

		case 5:
			system(FLUSH_STRING);
			if (!m_tuiter.isLoggedIn()) {
				printf("Erro: Voce nao fez login.\n");
				break;
			}

			m_tuiter.getActiveUser()->printMentions();
			break;

		case 6:
			printf("Digite a hashtag a ser pesquisada (sem o caractere #): ");
			std::cin >> buffer;
			system(FLUSH_STRING);
			m_tuiter.printHashtags(buffer);
			break;

		case 7:
			printf("Digite o texto a ser pesquisado: ");
			std::cin >> buffer;
			system(FLUSH_STRING);
			m_tuiter.search(buffer);
			break;

		case 8:
			if (!m_tuiter.isLoggedIn()) {
				system(FLUSH_STRING);
				printf("Erro: Voce nao fez login.\n");
				break;
			}
			m_tuiter.logoff();
			system(FLUSH_STRING);
			printf("Voce fez logoff.\n");
			break;

		case 9: // Auto-teste
			system(FLUSH_STRING);
			// Register users
			printf("Registering user 'abc123' with pass 'hello'\n");
			m_tuiter.registerUser("abc123", "hello");
			printf("Registering user 'testman' with pass 'howdy'\n");
			m_tuiter.registerUser("testman", "howdy");
			printf("Registering user 'FeiraModerna' with pass 'wassup'\n");
			m_tuiter.registerUser("FeiraModerna", "wassup");


			// First test case: abc123
			printf("Logging in to 'abc123'\n");
			if (m_tuiter.login("abc123", "hello"))
			{
				printf("Logged in as %s.\n", m_tuiter.loggedInAs().c_str());
				printf("Posting six messages\n");
				m_tuiter.post("Testando! #test");
				m_tuiter.post("Testando #TeSt mais uma vez! #test");
				m_tuiter.post("Testando #test pra caramba!");
				m_tuiter.post("Segunda-feira eh um dia complicado...");
				m_tuiter.post("Terca-feira tambem.");
				m_tuiter.post("Mas sexta-feira eh de boas.");

				printf("Showing timeline\n\n");
				m_tuiter.getActiveUser()->printTimeline();

				printf("Logging off\n");
				m_tuiter.logoff();
			}
			else printf("\tLogin error.\n");


			// Second test case: testman
			printf("Logging in to 'testman'\n");
			if (m_tuiter.login("testman", "howdy"))
			{
				printf("Logged in as %s.\n", m_tuiter.loggedInAs().c_str());
				printf("Posting a message\n");
				m_tuiter.post("Testando! E tambem mencionando @abc123. Eu @testman tb me menciono. #Test");
				m_tuiter.post("@abc123 Tambem acho segunda-feira complicado!");
				m_tuiter.post("@abc123 Sexta-feira eh dia de baladinha pra voce? HUEHUEHUEHUE");

				printf("Showing timeline\n\n");
				m_tuiter.getActiveUser()->printTimeline();

				printf("Logging off\n");
				m_tuiter.logoff();
			}
			else printf("\tLogin error.\n");


			// Third test case: FeiraModerna
			printf("Logging in to 'FeiraModerna'\n");
			if (m_tuiter.login("FeiraModerna", "wassup"))
			{
				printf("Posting a message\n");
				m_tuiter.post("@abc123 @testman A melhor Feira eh a Moderna. Garanto.");

				printf("Showing timeline\n\n");
				m_tuiter.getActiveUser()->printTimeline();

				printf("Logging off\n");
				m_tuiter.logoff();
			}
			else printf("\tLogin error.\n");


			// Fourth test case: abc123's mentions
			printf("Logging in to 'abc123' again\n");
			if (m_tuiter.login("abc123", "hello"))
			{
				printf("Logged in as %s.\n", m_tuiter.loggedInAs().c_str());
				printf("Showing all mentions\n\n");
				m_tuiter.getActiveUser()->printMentions();
				m_tuiter.logoff();
			}
			else printf("\tLogin error.\n");


			// Fifth test case: #test hashtag
			printf("Looking for all occurrences of #TeSt...\n");
			m_tuiter.printHashtags("TeSt");


			// Sixth test case: look for all occurrences of "feira"
			printf("Looking for all occurrences of text \"feira\"...\n");
			m_tuiter.search("feira");

			printf("All done.\n");
			printf("Por favor, reinicie o programa.\n");
			break;
		}

	} while (option != 0);

    return 0;
}
