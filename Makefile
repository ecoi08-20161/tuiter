
CXX = g++ --std=c++11
executable = bin/Debug/tuiter

.PHONY: clear delete dirs

all: dirs main.o tuiter.o usuario.o postagem.o timestamp.o avltree.o savetool.o
	$(CXX) main.o tuiter.o usuario.o postagem.o timestamp.o avltree.o savetool.o -o $(executable)
	@echo "Build successful."

dirs:
	@mkdir -p bin
	@mkdir -p bin/Debug
	@mkdir -p bin/Release

main.o: main.cpp
	$(CXX) -c main.cpp -o main.o

tuiter.o: tuiter.cpp
	$(CXX) -c tuiter.cpp -o tuiter.o

usuario.o: usuario.cpp
	$(CXX) -c usuario.cpp -o usuario.o
	
postagem.o: postagem.cpp
	$(CXX) -c postagem.cpp -o postagem.o

timestamp.o: timestamp.cpp
	$(CXX) -c timestamp.cpp -o timestamp.o

avltree.o: avltree.cpp
	$(CXX) -c avltree.cpp -o avltree.o

savetool.o: savetool.cpp
	$(CXX) -c savetool.cpp -o savetools.o

clean:
	rm -rf *.o
	
delete:
	rm -f $(executable)
