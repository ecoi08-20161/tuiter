#ifndef TUITER_HPP
#define TUITER_HPP

#include "usuario.hpp"
#include "postagem.hpp"
#include <vector>
#include <string>
#include <map>
#include "avltree.hpp"

class tuiter
{
	private:
		// Lista de usuários
		AVLTree m_usertree;

		// Localização de hashtags
		// Indexando listas de postagens contendo hashtag, por hashtag
		std::map< std::string, std::vector<postagem*> > m_hashtags;

		usuario* m_loggedInUser;
		
		// Menções
		void indexMentionsAndHashtags(postagem* myMessage);

	public:
		tuiter();
		~tuiter();
		// Register
		bool registerUser(std::string username, std::string pass);
		// Login, logoff
		bool login(std::string username, std::string pass);
		void logoff();
		void post(char*);
		void repost(char*, TimeStamp::TimeStampData);

		std::string loggedInAs();
		bool        isLoggedIn();
		usuario*    getActiveUser() const;

		// Gera hash para strings
		//static unsigned int generateStringHash(const char* string, int hash = 0);

		// Exibe listas de ocorrência de uma hashtag específica
		void printHashtags(std::string);

		// Encontra substring em nomes de usuários e em tweets
		void search(std::string);
};

#endif
