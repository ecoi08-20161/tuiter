#include "timestamp.hpp"
#include <ctime>
#include <cstdlib>
#include <cstdio>

TimeStamp::TimeStamp()
{
    //getTime();
}


void TimeStamp::getTime()
{
    time_t now = time(0);
	tm timestruct;
	char buffer[80];
	timestruct = *localtime(&now);

	strftime(buffer, sizeof(buffer), "%d", &timestruct);
	timeStamp.day    = static_cast<unsigned int>(strtol(buffer, nullptr, 0));
	strftime(buffer, sizeof(buffer), "%m", &timestruct);
	timeStamp.month  = static_cast<unsigned int>(strtol(buffer, nullptr, 0));
    strftime(buffer, sizeof(buffer), "%Y", &timestruct);
    timeStamp.year   = static_cast<unsigned int>(strtol(buffer, nullptr, 0));
    strftime(buffer, sizeof(buffer), "%H", &timestruct);
    timeStamp.hour   = static_cast<unsigned int>(strtol(buffer, nullptr, 0));
    strftime(buffer, sizeof(buffer), "%M", &timestruct);
    timeStamp.minute = static_cast<unsigned int>(strtol(buffer, nullptr, 0));
}

TimeStamp& TimeStamp::operator=(const TimeStamp& ts)
{
    if(!(this == &ts)) {
        timeStamp = ts.timeStamp;
    }
    return (*this);
}

bool TimeStamp::operator>(const TimeStamp& ts)
{
    if(&ts == this) return false;
    return (timeStamp.year  > ts.timeStamp.year
        && timeStamp.month  > ts.timeStamp.month
        && timeStamp.day    > ts.timeStamp.day
        && timeStamp.hour   > ts.timeStamp.hour
        && timeStamp.minute > ts.timeStamp.minute);

}

bool TimeStamp::operator<(const TimeStamp& ts)
{
    if(&ts == this) return false;
    return (timeStamp.year  < ts.timeStamp.year
        && timeStamp.month  < ts.timeStamp.month
        && timeStamp.day    < ts.timeStamp.day
        && timeStamp.hour   < ts.timeStamp.hour
        && timeStamp.minute < ts.timeStamp.minute);
}

bool TimeStamp::operator>=(const TimeStamp& ts)
{
    if(&ts == this) return true;
    return (timeStamp.year  >= ts.timeStamp.year
        && timeStamp.month  >= ts.timeStamp.month
        && timeStamp.day    >= ts.timeStamp.day
        && timeStamp.hour   >= ts.timeStamp.hour
        && timeStamp.minute >= ts.timeStamp.minute);
}

bool TimeStamp::operator<=(const TimeStamp& ts)
{
    if(&ts == this) return true;
    return (timeStamp.year  <= ts.timeStamp.year
        && timeStamp.month  <= ts.timeStamp.month
        && timeStamp.day    <= ts.timeStamp.day
        && timeStamp.hour   <= ts.timeStamp.hour
        && timeStamp.minute <= ts.timeStamp.minute);
}

bool TimeStamp::operator==(const TimeStamp& ts)
{
    if(&ts == this) return true;
    return (timeStamp.year  == ts.timeStamp.year
        && timeStamp.month  == ts.timeStamp.month
        && timeStamp.day    == ts.timeStamp.day
        && timeStamp.hour   == ts.timeStamp.hour
        && timeStamp.minute == ts.timeStamp.minute);
}

std::string TimeStamp::toString()
{
    char buffer[80];
    sprintf(buffer, "%d/%d/%d %d:%d",
                    timeStamp.day,
                    timeStamp.month,
                    timeStamp.year,
                    timeStamp.hour,
                    timeStamp.minute);
    return buffer;
}