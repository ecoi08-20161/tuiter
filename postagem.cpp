#include "postagem.hpp"

postagem::postagem()
{
    m_parent = NULL;
    strcpy(m_texto," ");
}

postagem::~postagem() {}

char* postagem::getText() {
	return m_texto;
}

usuario* postagem::getParent() {
	return m_parent;
}

TimeStamp postagem::getTimestamp()
{
	return m_timestamp;
}

void postagem::setText(char _text[])
{
    strcpy(m_texto, _text);
	m_timestamp.getTime();
}

void postagem::setParent(usuario* _parent)
{
    m_parent = _parent;
}

void postagem::setTimestampData(TimeStamp::TimeStampData tsd) {
	m_timestamp.timeStamp = tsd;
}
