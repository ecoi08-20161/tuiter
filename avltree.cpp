#include "avltree.hpp"

void AVLTree::_recursiveClear(AVLTree::Node*& n)
{
    if(!n) return;
    if(n->left)  _recursiveClear(n->left);
    if(n->right) _recursiveClear(n->right);
    Remove(n->key);
    n = nullptr;
}

usuario* AVLTree::_find(std::string key, AVLTree::Node* n)
{
    // Só retorne alguma coisa se o nó não for nulo.
    if(n)
    {
        // Caso a chave seja encontrada, retorne a informação
        if(n->key == key) return n->info;
        // Caso a chave seja menor que a chave do nó, vá para a esquerda.
        // O mesmo vale para um valor maior e a direita.
        return _find(key, key < n->key ? n->left : n->right);
    }
    return 0;
}

void AVLTree::_add(AVLTree::Node*& n, std::string key, usuario* info)
{
    if(!n)
    {
        n = new AVLTree::Node;
        n->key = key;
        n->info = info;
        // Left e Right são nullptr por default
        // fator de balancemento é 0 por default
        return;
    }

    if(key < n->key) _add(n->left,  key, info);
    else             _add(n->right, key, info);
}

void AVLTree::_remove(AVLTree::Node*& n, std::string key)
{
    if(!n) return;
    if(n->key == key) _deleteNode(n);
    else if(n->key < key) _remove(n->left, key);
    else _remove(n->right, key);
}

void AVLTree::_deleteNode(AVLTree::Node*& n)
{
    // Caso sem sub-árvore
    if(!n->left && !n->right)
    {
        delete n;
        n = nullptr; // Nó é referência vinda do pai
        return;
    }

    // Casos de sub-árvore
    if(n->left && !n->right)
        n = n->left;
    else if(!n->left && n->right)
        n = n->right;
    else
    {
        AVLTree::Node* aux = n->left;
        AVLTree::Node* parent = n;
        while(aux->right)
        {
            parent = aux;
            aux = aux->right;
        }
        parent->right = aux->left;
        aux->left = n->left;
        aux->right = n->right;
        AVLTree::Node* aux2 = n;
        delete aux2;
        n = aux;
        return;
    }
    delete n;
    n = nullptr;
}

long long int AVLTree::_getHeight(const AVLTree::Node* n)
{
    long long int Hl, Hr;
    if(!n) return 0L;
    Hl = _getHeight(n->left);
    Hr = _getHeight(n->right);
    return 1L + ((Hl > Hr) ? Hl : Hr);
}

void AVLTree::_evaluateBalanceFactor(AVLTree::Node*& n)
{
    if(!n) return;
    n->fb = static_cast<int>(_getHeight(n->left) - _getHeight(n->right));
}

void AVLTree::_evaluateFactorsRecursively(AVLTree::Node*& n)
{
    _evaluateBalanceFactor(n);
    if(!n) return;
    _evaluateFactorsRecursively(n->left);
    _evaluateFactorsRecursively(n->right);
}

void AVLTree::_balance(AVLTree::Node*& n)
{
    if(!n) return;
    _evaluateBalanceFactor(n);
    if(n->fb == 2) {
        _evaluateBalanceFactor(n->left);
        if(n->left->fb > 0) _rightRotation(n);
        else                _doubleRightRotation(n);
    }
    else if(n->fb == -2) {
        _evaluateBalanceFactor(n->right);
        if(n->right->fb < 0) _leftRotation(n);
        else                 _doubleLeftRotation(n);
    }
    _balance(n->left);
    _balance(n->right);
}

void AVLTree::_rightRotation(AVLTree::Node*& n)
{
    AVLTree::Node* aux = n->left;
    n->left = aux->right;
    aux->right = n;
    n = aux;
}

void AVLTree::_leftRotation(AVLTree::Node*& n)
{
    AVLTree::Node* aux = n->right;
    n->right = aux->left;
    aux->left = n;
    n = aux;
}

void AVLTree::_doubleRightRotation(AVLTree::Node*& n)
{
    AVLTree::Node* aux  = n->left;
    AVLTree::Node* aux2 = aux->right;
    aux->right = aux2->left;
    n->left = aux2->right;
    aux2->left = aux;
    aux2->right = n;
    n = aux2;
}

void AVLTree::_doubleLeftRotation(AVLTree::Node*& n)
{
    AVLTree::Node* aux  = n->right;
    AVLTree::Node* aux2 = aux->left;
    n->right = aux2->left;
    aux->left = aux2->right;
    aux2->left = n;
    aux2->right = aux;
    n = aux2;
}

AVLTree::AVLTree()
{
    root = nullptr;
	n_elems = 0u;
}

AVLTree::~AVLTree()
{
	Clear();
}

void AVLTree::Add(std::string key, usuario* info)
{
    _add(root, key, info);
	n_elems++;
    _balance(root);
    _evaluateFactorsRecursively(root);
}

void AVLTree::Remove(std::string key)
{
    _remove(root, key);
	n_elems--;
    _balance(root);
    _evaluateFactorsRecursively(root);
}

void AVLTree::Clear()
{
    _recursiveClear(root);
}

usuario* AVLTree::Find(std::string key)
{
    return _find(key, root);
}

bool AVLTree::IsEmpty()
{
    return (root == nullptr);
}

unsigned int AVLTree::size()
{
	return n_elems;
}

AVLTree::Node* AVLTree::getRoot() {
	return root;
}