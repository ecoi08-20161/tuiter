#include "savetool.hpp"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <functional>

void SaveTool::Save(const char * file, AVLTree& myTree)
{
	FILE* fp;
	fp = fopen(file, "wb");
	if (!fp) printf("Impossivel salvar o arquivo!\n");

	// Salvar n�mero de usu�rios
	unsigned int n_users = myTree.size();
	fwrite(&n_users, sizeof(unsigned int), 1, fp);


	// Lambda para salvar lista de usu�rios
	// Como � uma fun��o recursiva, vale a pena cri�-la aqui dentro ao inv�s de fora.
	std::function<void(AVLTree::Node*)> _myExportFunc =
		[&](AVLTree::Node* n)
		{
			if (!n) return;
			SaveTool::UserSave usv;
			strcpy(usv.username, n->info->getUsername().c_str());
			strcpy(usv.password, n->info->getEncryptedPassword().c_str());
			usv.n_posts = n->info->getPostList()->size();

			// Escreva dados do usu�rio
			fwrite(&usv, sizeof(SaveTool::UserSave), 1, fp);

			// Importe as postagens do mesmo
			if (usv.n_posts > 0)
			{
				SaveTool::PostSave* ps = new SaveTool::PostSave[usv.n_posts];
				int i = 0;
				for (auto post : *n->info->getPostList())
				{
					strcpy(ps[i].message, post->getText());
					ps[i].ts = post->getTimestamp().timeStamp;
					i++;
				}

				// Salve as postagens
				fwrite(ps, sizeof(SaveTool::PostSave), usv.n_posts, fp);
				delete[] ps;
			}
			// Fa�a o mesmo para esquerda e direita
			_myExportFunc(n->left);
			_myExportFunc(n->right);
		};

	// Chamando o lambda recursivamente para a raiz da �rvore AVL
	_myExportFunc(myTree.getRoot());

	fclose(fp);
}

void SaveTool::Load(const char* file, tuiter& t)
{
	FILE* fp = fopen(file, "rb");

	if (!fp) return;

	unsigned int n_users;
	fread(&n_users, sizeof(unsigned int), 1, fp);

	while (n_users > 0)
	{
		SaveTool::UserSave usv;
		fread(&usv, sizeof(SaveTool::UserSave), 1, fp);

		// Re-register user
		t.registerUser(usv.username, usv.password);
		// Login
		t.login(usv.username, usv.password);
		// Post
		while (usv.n_posts > 0)
		{
			SaveTool::PostSave psv;
			fread(&psv, sizeof(SaveTool::PostSave), 1, fp);
			t.repost(psv.message, psv.ts);
			usv.n_posts--;
		}
		t.logoff();
		n_users--;
	}

	fclose(fp);
}
